using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RandomColorAssigner : MonoBehaviour
{
    [SerializeField] private GameObject[] _cubes;
    [SerializeField] private Color[] _colors;


    private void Awake()
    {
        for(int i=0; i < 3; i++)
        {
            var randomNum = Random.Range(0, _colors.Length);
            var color = _colors[randomNum];
            var cube = _cubes[i];
            var cubeMat = cube.GetComponent<MeshRenderer>();
            cubeMat.material.color = color;
        }
    }

    public Material leftcubecolor()
    {
        var color = _cubes[0].gameObject.GetComponent<MeshRenderer>().material;
        return color;
    }
    public Material middlecubecolor()
    {
        var color = _cubes[1].gameObject.GetComponent<MeshRenderer>().material;
        return color;
    }
    public Material rightcubecolor()
    {
        var color = _cubes[2].gameObject.GetComponent<MeshRenderer>().material;
        return color;
    }
}
