using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratingCubes : MonoBehaviour
{
    [SerializeField] private GameObject leftcube;
    [SerializeField] private GameObject middlecube;
    [SerializeField] private GameObject rightcube;

    [SerializeField] private GameObject leftPos;
    [SerializeField] private GameObject midPos;
    [SerializeField] private GameObject rightPos;


    public void GenerateLeftCube()
    {
        var cube = Instantiate(leftcube, leftPos.transform.position,Quaternion.identity);
        cube.AddComponent<Rigidbody>();
        cube.transform.localScale = new Vector3(.5f,.5f,.5f);
    }
    public void GenerateMiddleCube()
    {
        var cube = Instantiate(middlecube, midPos.transform.position, Quaternion.identity);
        cube.AddComponent<Rigidbody>();
        cube.transform.localScale = new Vector3(.5f, .5f, .5f);
    }
    public void GenerateRightCube()
    {
        var cube = Instantiate(rightcube, rightPos.transform.position, Quaternion.identity);
        cube.AddComponent<Rigidbody>();
        cube.transform.localScale = new Vector3(.5f, .5f, .5f);
    }
}
